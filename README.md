## INTRODUCTION

The search_api_solr_densevector module is a proof of concept that demonstrates how Solr Dense Vector fields could be integrated with the Search API Solr module for Drupal. This allows for neural searches, and potentially other applications like RAG. However, for the time being this solution is limited in usefulness until Solr (Lucene) supports multi-value dense vector fields. Ideally a Drupal body field is chunked into sentences, or paragraphs, before the text is converted to a vector. Creating multiple vectors that represent a blog post for example. Right now Solr doesn't support storing multiple vectors in the same field which significantly complicates the architecture of a Solr index with vectors.

It is potentially useful if you're only needing to vectorize a small chunk of text, or images, or user personalization data, but for generic integration with the existing Search API architecture it's not a good fit ... yet.

For more details about implementation and progress see https://www.drupal.org/project/search_api_solr/issues/3397950

## REQUIREMENTS

This module requires:

- The latest versions of Search API & Search API Solr contributed modules
- Search API Solr module must have the ./search_api_solr-dense-vector.patch patch applied.
- A Solr 9.1+ server
- A Hugging Face inference API key

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

Add your Hugging Face API key in settings.php:

```php
$settings['search_api_solr_densevector_hugging_face_key'] = '';
```
