<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\search_api_solr_densevector\Utility\StringHelper;
use Drupal\search_api_solr_densevector\Utility\TextChunker;
use Psr\Http\Client\ClientInterface;

/**
 * Hugging face embeddings service.
 */
class HuggingFaceEmbeddings implements HuggingFaceEmbeddingsInterface {

  /**
   * Hugging face access key.
   *
   * @var string
   */
  protected string $hfKey = '';

  /**
   * Hugging face sentence-transformers model to use.
   *
   * @var string
   */
  protected string $model = 'sentence-transformers/all-MiniLM-L6-v2';

  /**
   * Hugging face base url.
   *
   * @var string
   */
  protected string $urlBase = 'https://api-inference.huggingface.co/pipeline/feature-extraction/';

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs a HuggingFaceEmbeddings object.
   */
  public function __construct(
    private readonly ClientInterface $httpClient,
    private readonly CacheFactoryInterface $cacheFactory,
  ) {
    $this->cache = $this->cacheFactory->get('hugging_face_embeddings');
    // This this in settings.php as:
    // $settings['search_api_solr_densevector_hugging_face_key'] = '';.
    $this->hfKey = Settings::get('search_api_solr_densevector_hugging_face_key', '');
  }

  /**
   * {@inheritdoc}
   */
  public function getEmbeddings($text): array|bool {
    $cid = md5($text);
    $data = $this->cache->get($cid);

    if (!empty($data)) {
      return $data->data;
    }
    else {
      // @todo Make configurable.
      $chunkMaxSize = 1024;
      $chunkMinOverlap = 64;

      $chunks = TextChunker::chunkText($text, $chunkMaxSize, $chunkMinOverlap);

      foreach ($chunks as $key => $chunk) {
        $chunks[$key] = StringHelper::prepareText($chunk, [], $chunkMaxSize);
      }

      $body = [
        'inputs' => $chunks,
        'options' => [
          'wait_for_module' => TRUE,
        ],
      ];

      $options = [
        'headers' => [
          'Authorization' => "Bearer $this->hfKey",
        ],
        'body' => json_encode($body),
      ];

      try {
        $response = $this->httpClient->post($this->urlBase . $this->model, $options);
      }
      catch (\Exception $e) {
        // Error handling ...
      }

      if ($response->getStatusCode() === 200) {
        $embeddings = json_decode($response->getBody()->getContents());
        $this->cache->set($cid, $embeddings);
        return $embeddings;
      }
      else {
        // Error handling ...
      }
    }

    return FALSE;

  }

}
