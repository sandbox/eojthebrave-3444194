<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector;

/**
 * Interface for hugging face embeddings service.
 *
 * Retrieve vector embeddings for a string of text from the Hugging Face API.
 */
interface HuggingFaceEmbeddingsInterface {

  /**
   * Get embeddings.
   *
   * @param string $text
   *   Text to get embeddings for.
   *
   * @return array|bool
   *   An array of vectors or FALSE on error.
   */
  public function getEmbeddings(string $text): array|bool;

}
