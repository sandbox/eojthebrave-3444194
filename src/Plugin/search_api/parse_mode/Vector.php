<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\Plugin\search_api\parse_mode;

use Drupal\search_api\Plugin\search_api\parse_mode\Phrase;

/**
 * Represents a parse mode that parses the input into a vector.
 *
 * @SearchApiParseMode(
 *   id = "vector",
 *   label = @Translation("Multiple words as vector"),
 *   description = @Translation("The query string is converted to a vector. Solr will perform a Dense Vector (neural) search."),
 * )
 */
class Vector extends Phrase {

}
