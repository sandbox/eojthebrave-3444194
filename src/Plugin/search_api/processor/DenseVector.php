<?php

namespace Drupal\search_api_solr_densevector\Plugin\search_api\processor;

use Drupal\search_api\Plugin\search_api\data_type\value\TextToken;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api_solr_densevector\HuggingFaceEmbeddingsInterface;
use Drupal\search_api_solr_densevector\Plugin\search_api\data_type\value\DenseVectorValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add date ranges to the index.
 *
 * @SearchApiProcessor(
 *   id = "solr_densevector",
 *   label = @Translation("DenseVector"),
 *   description = @Translation("DenseVectors."),
 *   stages = {
 *     "preprocess_index" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class DenseVector extends ProcessorPluginBase {

  /**
   * Hugging Face embeddings service.
   *
   * @var \Drupal\search_api_solr_densevector\HuggingFaceEmbeddingsInterface
   */
  protected HuggingFaceEmbeddingsInterface $huggingFaceEmbeddings;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, HuggingFaceEmbeddingsInterface $huggingFaceEmbeddings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->huggingFaceEmbeddings = $huggingFaceEmbeddings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('search_api_solr_densevector.hugging_face_embeddings'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessIndexItems(array $items) {
    // For testing/debugging the indexing process we can skip the call to
    // Hugging Face and just generate a random array that looks enough like a
    // vector.
    $skip = FALSE;

    foreach ($items as $item) {
      foreach ($item->getFields() as $field) {
        if ('solr_densevector' === $field->getType()) {
          $vectors = [];

          if (!$skip) {
            foreach ($field->getValues() as $value) {
              if (is_array($value)) {
                $text = '';
                foreach ($value as $v) {
                  $text .= $v instanceof TextToken ? $v->getText() : $v;
                }
              }
              else {
                $text = $value;
              }

              if (!empty($text)) {
                $embeddings = $this->huggingFaceEmbeddings->getEmbeddings($text);
                $vectors[] = new DenseVectorValue($embeddings[0]);
              }
            }
          }
          else {
            // Use dummy data for testing purposes.
            $randomArray = [];
            for ($i = 0; $i < 384; $i++) {
              $randomArray[] = mt_rand() / mt_getrandmax();
            }
            $vectors[] = new DenseVectorValue($randomArray);

          }

          if (count($vectors)) {
            $field->setValues($vectors);
          }
        }
      }
    }
  }

}
