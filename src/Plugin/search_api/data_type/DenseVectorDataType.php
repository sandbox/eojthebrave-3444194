<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a dense vector data type.
 *
 * @SearchApiDataType(
 *   id = "solr_densevector",
 *   label = @Translation("DenseVector"),
 *   description = @Translation("Vector representation of the text."),
 *   prefix = "knn"
 * )
 */
class DenseVectorDataType extends DataTypePluginBase {}
