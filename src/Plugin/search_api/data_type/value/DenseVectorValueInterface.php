<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\Plugin\search_api\data_type\value;

/**
 * Provides an interface for vector field values.
 */
interface DenseVectorValueInterface {

  /**
   * Return the vector values.
   *
   * @return array
   *   An array of vectors for the field.
   */
  public function getVectors() : array;

}
