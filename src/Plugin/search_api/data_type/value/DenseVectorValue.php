<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\Plugin\search_api\data_type\value;

/**
 * Represents a single date range value.
 */
class DenseVectorValue implements DenseVectorValueInterface {

  /**
   * The vector values.
   *
   * @var array
   */
  protected $vectors;

  /**
   * Constructs a DenseVectorValue object.
   *
   * @param array $vectors
   *   The vectors.
   */
  public function __construct(array $vectors) {
    $this->vectors = $vectors;
  }

  /**
   * {@inheritdoc}
   */
  public function getVectors() : array {
    return $this->vectors;
  }

}
