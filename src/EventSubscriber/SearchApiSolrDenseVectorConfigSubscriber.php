<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\EventSubscriber;

use Drupal\search_api_solr\Event\PostConfigFilesGenerationEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alters the query where necessary to implement business logic.
 *
 * @package Drupal\<your_module_name>\EventSubscriber
 */
class SearchApiSolrDenseVectorConfigSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiSolrEvents::POST_CONFIG_FILES_GENERATION => 'postConfigFilesGeneration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function postConfigFilesGeneration(PostConfigFilesGenerationEvent $event): void {
    $files = $event->getConfigFiles();
    if (isset($files['schema_extra_fields.xml'])) {
      $knn_field = '<dynamicField name="knn_*" type="knn_vector" stored="true" indexed="true" multiValued="false" />';
      $knn_field .= PHP_EOL . '<dynamicField name="knns_*" type="knn_vector" stored="true" indexed="true" multiValued="false" />';
      $files['schema_extra_fields.xml'] = preg_replace('/(^.*(type="knn_vector").*(multiValued="true").*$\n)/m', '', $files['schema_extra_fields.xml']);
      $files['schema_extra_fields.xml'] = $knn_field . PHP_EOL . $files['schema_extra_fields.xml'];
    }
    $event->setConfigFiles($files);
  }

}
