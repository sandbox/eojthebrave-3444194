<?php

declare(strict_types=1);

namespace Drupal\search_api_solr_densevector\EventSubscriber;

use Drupal\search_api_solr\Event\PostConvertedQueryEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Drupal\search_api_solr_densevector\HuggingFaceEmbeddingsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alters the query where necessary to implement business logic.
 *
 * @package Drupal\<your_module_name>\EventSubscriber
 */
class SearchApiSolrDenseVectorQuerySubscriber implements EventSubscriberInterface {

  /**
   * Hugging face embeddings service.
   *
   * @var \Drupal\search_api_solr_densevector\HuggingFaceEmbeddingsInterface
   */
  protected $huggingFaceEmbeddings;

  /**
   * SearchApiSolrDenseVectorQuerySubscriber constructor.
   *
   * @param \Drupal\search_api_solr_densevector\HuggingFaceEmbeddingsInterface $huggingFaceEmbeddings
   *   Hugging face embeddings service.
   */
  public function __construct(HuggingFaceEmbeddingsInterface $huggingFaceEmbeddings) {
    $this->huggingFaceEmbeddings = $huggingFaceEmbeddings;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiSolrEvents::POST_CONVERT_QUERY => 'postConvertQuery',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function postConvertQuery(PostConvertedQueryEvent $event): void {
    $query = $event->getSearchApiQuery();
    $solarium_query = $event->getSolariumQuery();

    if ($solarium_query->getType() === 'select') {
      $fields = $query->getIndex()->getFields();
      $vector_fields = [];
      foreach ($fields as $key => $field) {
        if ($field->getType() == 'solr_densevector') {
          $vector_fields[$key] = $field;
        }
      }

      if (count($vector_fields) && $keys = $query->getOriginalKeys()) {
        // Get the search keys entered by the user.
        $keys_vector = $this->huggingFaceEmbeddings->getEmbeddings($keys);
        $solr_fields = [];
        /** @var \Drupal\search_api\Index\Field $field */
        foreach ($vector_fields as $field) {
          $solr_fields[] = 'knns_' . $field->getFieldIdentifier();
        }

        $knn_query = '{!knn f=' . implode(',', $solr_fields) . ' topK=25}[' . implode(', ', $keys_vector[0]) . ']';
        $solarium_query->setQuery($knn_query);
      }
    }
  }

}
